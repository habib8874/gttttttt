from __future__ import unicode_literals

import frappe

def get_context(context):
	# do your magic here
	applicant_name = frappe.get_value("Job Applicant", {'email_id': frappe.session.user},"name")
	frappe.form_dict.applicant_name = applicant_name
	frappe.form_dict.new = 0
	context.doc = frappe.get_doc("Job Applicant",applicant_name)
	pass
