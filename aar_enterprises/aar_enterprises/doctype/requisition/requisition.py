from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
from frappe.utils import getdate, nowdate, cint, flt

@frappe.whitelist()
def  get_active_staffing_plan_details_s(company, designation, from_date=getdate(nowdate()), to_date=getdate(nowdate())):
	if not company or not designation:
		frappe.throw(_("Please select Company and Designation"))

	staffing_plan = frappe.db.sql("""
		select sp.name, spd.vacancies, spd.total_estimated_cost,spd.available_positions,spd.estimated_cost_per_position
		from `tabStaffing Plan Detail` spd join `tabStaffing Plan` sp on spd.parent=sp.name
		where company=%s and spd.designation=%s and sp.docstatus=1
		and to_date >= %s and from_date <= %s """, (company, designation, from_date, to_date), as_dict = 1)

	if not staffing_plan:
		parent_company = frappe.get_cached_value('Company',  company,  "parent_company")
		if parent_company:
			staffing_plan = get_active_staffing_plan_details_s(parent_company,
				designation, from_date, to_date)

	# Only a single staffing plan can be active for a designation on given date
	return staffing_plan if staffing_plan else None