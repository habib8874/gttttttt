frappe.ui.form.on('Job Opening', {
    validate:function(frm)
{
    let d = new frappe.ui.Dialog({
    title: 'Add Comments on Status',
    fields: [
        {
            label: 'Designation',
            fieldname: 'comment',
            fieldtype: 'Text'
        },
    ],
    primary_action_label: 'Submit',
    primary_action(values) {
        
        frappe.call({
                "method": "frappe.desk.form.utils.add_comment",
                "args": {
                        reference_doctype: frm.doctype,
                        reference_name: frm.docname,
                        content: values.comment,
                        comment_email: frappe.session.user,
                        comment_by: frappe.session.user_fullname,
                    }
                });
         d.hide();
    }
});

d.show();

},
designation: function(frm) {
		if(frm.doc.designation && frm.doc.company){
			frappe.call({
				"method": "aar_enterprises.aar_enterprises.doctype.requisition.requisition.get_active_staffing_plan_details_s",
				args: {
					company: frm.doc.company,
					designation: frm.doc.designation,
					date: frappe.datetime.now_date() // ToDo - Date in Job Opening?
				},
				callback: function (data) {
					if(data.message){
						frm.set_value('staffing_plan', data.message[0].name);
						frm.set_value('planned_vacancies', data.message[0].vacancies);
						frm.set_value('available_position', data.message[0].available_positions);
						frm.set_value('estimated_cost_per_position', data.message[0].estimated_cost_per_position);
						frm.set_value('available_cost_per_position',(data.message[0].available_positions*data.message[0].estimated_cost_per_position))
						
					} else {
						frm.set_value('staffing_plan', "");
						frm.set_value('planned_vacancies', 0);
						frappe.show_alert({
							indicator: 'orange',
							message: __('No Staffing Plans found for this Designation')
						});
					}
				}
			});
		}
		else{
			frm.set_value('staffing_plan', "");
			frm.set_value('planned_vacancies', 0);
		}
	},
	company: function(frm) {
		frm.set_value('designation', "");
	}
});
