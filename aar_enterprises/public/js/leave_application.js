


frappe.ui.form.on('Leave Application', {
    refresh: function (frm) {
        cur_frm.set_query("replacement", function () {
            return {
                "filters": {
                    "employee": ["not in", [frm.doc.employee]]
                }
            };
        });
    },
});



frappe.ui.form.on('Leave Application', {
    from_date: function (frm, cdt, cdn) {
        if (frm.doc.leave_type == "Annual Leave") {

            var myCurrentDate = new Date();
            var myFutureDate = new Date(myCurrentDate);
            myFutureDate.setDate(myFutureDate.getDate() + 30);

            var dd = String(myFutureDate.getDate()).padStart(2, '0');
            var mm = String(myFutureDate.getMonth() + 1).padStart(2, '0');
            var yyyy = myFutureDate.getFullYear();
            myFutureDate = yyyy + '-' + mm + '-' + dd;

            if (frm.doc.from_date < myFutureDate) {
                frappe.throw(__("For Annual Leave you've to apply 30 days before!"));
            }

        }

    },

    validate: function (frm) {
        if (frm.doc.leave_type == "Annual Leave") {

            var myCurrentDate = new Date();
            var myFutureDate = new Date(myCurrentDate);
            myFutureDate.setDate(myFutureDate.getDate() + 30);

            var dd = String(myFutureDate.getDate()).padStart(2, '0');
            var mm = String(myFutureDate.getMonth() + 1).padStart(2, '0');
            var yyyy = myFutureDate.getFullYear();
            myFutureDate = yyyy + '-' + mm + '-' + dd;

            if (frm.doc.from_date < myFutureDate) {
                frappe.throw(__("For Annual Leave you've to apply 30 days before!"));
            }

        }
    }
});



frappe.ui.form.on('Leave Application', {
    leave_balance: function (frm) {
        if (frm.doc.leave_balance == "0") {
            frappe.throw(__("You don't have any leave left on selected category. Please choose other category to proceed further."))
        }

    },

    validate: function (frm) {
        if (frm.doc.leave_balance == "0") {
            frappe.throw(__("You don't have any leave left on selected category. Please choose other category to proceed further."))

            validate = false
        }

    }
});


